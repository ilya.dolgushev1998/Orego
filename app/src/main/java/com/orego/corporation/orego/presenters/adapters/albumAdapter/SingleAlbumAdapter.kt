package com.orego.corporation.orego.presenters.adapters.albumAdapter

import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.orego.corporation.orego.R
import com.orego.corporation.orego.utils.KEY_PATH
import java.io.File
import java.util.*

class SingleAlbumAdapter(private val activity: Activity, private val data: ArrayList<HashMap<String, String>>) : BaseAdapter() {

    override fun getCount(): Int {
        return data.size
    }

    override fun getItem(position: Int): Any {
        return position
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        var convertedView = convertView
        val galleryImage: ImageView?
        if (convertedView == null) {

            convertedView = LayoutInflater.from(activity).inflate(
                    R.layout.album_single_element, parent, false)

            galleryImage = convertedView!!.findViewById(R.id.galleryImage)

            convertedView.tag = galleryImage
        } else {
            galleryImage = convertedView.tag as ImageView?
        }
        galleryImage!!.id = position

        val song: HashMap<String, String> = data[position]
        try {

            Glide.with(activity)
                    .load(File(song[KEY_PATH])) // Uri of the picture
                    .into(galleryImage)


        } catch (ignored: Exception) {
        }

        return convertedView
    }

}