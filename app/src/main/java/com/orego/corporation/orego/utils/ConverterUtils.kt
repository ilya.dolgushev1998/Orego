package com.orego.corporation.orego.utils

import android.annotation.SuppressLint
import android.content.Context
import com.orego.corporation.orego.views.KOEFF_A
import java.text.SimpleDateFormat
import java.util.*

@SuppressLint("SimpleDateFormat")
fun convertToTime(timestamp: String): String {
    val datetime = java.lang.Long.parseLong(timestamp)
    val date = Date(datetime)
    val formatter = SimpleDateFormat("dd/MM HH:mm")
    return formatter.format(date)
}

fun convertDpToPixel(dp: Float, context: Context): Float {
    val resources = context.resources
    val metrics = resources.displayMetrics
    return dp * (metrics.densityDpi / KOEFF_A)
}