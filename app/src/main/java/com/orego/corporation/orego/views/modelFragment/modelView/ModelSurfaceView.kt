package com.orego.corporation.orego.views.modelFragment.modelView

import android.annotation.SuppressLint
import android.opengl.GLSurfaceView
import android.view.MotionEvent
import com.orego.corporation.orego.presenters.MainActivity
import com.orego.corporation.orego.models.controllers.TouchController
import com.orego.corporation.orego.presenters.modelRender.ModelRender
import com.orego.corporation.orego.models.portrait.headComposition.HeadComposition

/**
 * GL customView, setRenderer,
 * set GL Version and control touchEvents
 */
@SuppressLint("ViewConstructor")
class ModelSurfaceView(private val mainActivity: MainActivity
                       , headComposition: HeadComposition) : GLSurfaceView(mainActivity) {

    private var touchHandler: TouchController

    init {
        setEGLContextClientVersion(3)
        val mRenderer = ModelRender(this, headComposition)
        setRenderer(mRenderer)
        touchHandler = TouchController(this, mRenderer)
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onTouchEvent(event: MotionEvent): Boolean {
        return touchHandler.onTouchEvent(event)
    }

    fun getMainActivity(): MainActivity {
        return mainActivity
    }
}