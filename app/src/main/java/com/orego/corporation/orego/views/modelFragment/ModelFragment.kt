package com.orego.corporation.orego.views.modelFragment

import android.annotation.SuppressLint
import android.graphics.Bitmap
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.ProgressBar
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.fragment.app.Fragment
import com.google.android.material.snackbar.Snackbar
import com.orego.corporation.orego.R
import com.orego.corporation.orego.models.portrait.headComposition.HeadComposition
import com.orego.corporation.orego.presenters.MainActivity
import com.orego.corporation.orego.models.loaders.loadersModel.LoadModel
import com.orego.corporation.orego.views.modelFragment.modelView.ModelSurfaceView

/**
 * Required to display the GLView.
 */
class ModelFragment : Fragment() {

    companion object {
        const val TAG = "MODEL_FRAGMENT"
    }

    private lateinit var constraintLayout: ConstraintLayout

    private lateinit var dialog: ProgressBar

    private lateinit var progressBarInsideText: TextView


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?
                              , savedInstanceState: Bundle?): View {
        constraintLayout = inflater.inflate(R.layout.progress_rendering, container
                , false) as androidx.constraintlayout.widget.ConstraintLayout
        dialog = constraintLayout.findViewById(R.id.progress_bar_model)
        progressBarInsideText = constraintLayout.findViewById(R.id.progressBarInsideText)
        return constraintLayout
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val mPicture = arguments!!.get("picture") as Bitmap
        val position = arguments!!.getInt("position")
        LoadModel(this, mPicture, position).execute()
    }

    fun setModel(headComposition: HeadComposition) {
        constraintLayout.addView(ModelSurfaceView(activity as MainActivity, headComposition))
    }

    @SuppressLint("SetTextI18n")
    fun initDialog() {
        this.dialog.visibility = View.VISIBLE
        this.progressBarInsideText.visibility = View.VISIBLE
        activity!!.window.setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
        this.progressBarInsideText.text = "Loading, please wait..."
    }

    fun updateDialog(charSequence: CharSequence) {
        this.progressBarInsideText.text = charSequence
    }

    fun closeDialog() {
        if (dialog.visibility == View.VISIBLE) {
            this.dialog.visibility = View.GONE
            this.progressBarInsideText.visibility = View.GONE
            activity!!.window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
        }
    }

    fun isDownload(): Boolean {
        return dialog.visibility == View.VISIBLE
    }

    fun showSnackBarWaiting() {
        Snackbar.make(constraintLayout, "Please, wait for Creating", Snackbar.LENGTH_SHORT).show()
    }
    fun showSnackBarError() {
        Snackbar.make(constraintLayout, "Ooops... something went wrong :(", Snackbar.LENGTH_SHORT).show()
    }
}