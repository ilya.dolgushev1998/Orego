package com.orego.corporation.orego.views

import org.intellij.lang.annotations.MagicConstant

@MagicConstant
const val KOEFF_A = 160f

@MagicConstant
const val KOEFF_B = 360

@MagicConstant
const val KOEFF_C = 17

@MagicConstant
const val KOEFF_HELF = 2