package com.orego.corporation.orego.models.loaders.loadersAlbum

import android.content.Context
import android.database.MergeCursor
import android.os.AsyncTask
import android.provider.MediaStore
import com.orego.corporation.orego.presenters.MainActivity
import com.orego.corporation.orego.models.loaders.loadersAlbum.supplier.MapComparator
import com.orego.corporation.orego.presenters.managers.AlbumManager
import com.orego.corporation.orego.utils.*
import java.util.*

class LoadAlbum(private val albumManager: AlbumManager, private val albumList: ArrayList<HashMap<String, String>>) : AsyncTask<String, Void, String>() {



    override fun onPreExecute() {
        super.onPreExecute()
        albumList.clear()
    }

    override fun doInBackground(vararg args: String): String {
        val xml = ""

        var path: String?
        var album: String?
        var timestamp: String?
        var countPhoto: String?
        val uriExternal = MediaStore.Images.Media.EXTERNAL_CONTENT_URI
        val uriInternal = MediaStore.Images.Media.INTERNAL_CONTENT_URI


        val projection = arrayOf(MediaStore.MediaColumns.DATA, MediaStore.Images.Media.BUCKET_DISPLAY_NAME, MediaStore.MediaColumns.DATE_MODIFIED)
        val mainActivity = this.albumManager.cameraFragment.activity as MainActivity
        val contentResolver = mainActivity.contentResolver
        val cursorExternal = contentResolver.query(uriExternal, projection, "_data IS NOT NULL) GROUP BY (bucket_display_name", null, null)
        val cursorInternal = contentResolver.query(uriInternal, projection, "_data IS NOT NULL) GROUP BY (bucket_display_name", null, null)
        val cursor = MergeCursor(arrayOf(cursorExternal, cursorInternal))

        while (cursor.moveToNext()) {

            path = cursor.getString(cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA))
            album = cursor.getString(cursor.getColumnIndexOrThrow(MediaStore.Images.Media.BUCKET_DISPLAY_NAME))
            timestamp = cursor.getString(cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATE_MODIFIED))
            countPhoto = getCount(mainActivity.applicationContext, album)

            albumList.add(mappingInbox(album, path, timestamp, convertToTime(timestamp), countPhoto))
        }
        cursor.close()
        Collections.sort(albumList, MapComparator(KEY_TIMESTAMP, "dsc")) // Arranging photo album by timestamp decending
        return xml
    }

    private fun getCount(c: Context, album_name: String): String {
        val uriExternal = MediaStore.Images.Media.EXTERNAL_CONTENT_URI
        val uriInternal = MediaStore.Images.Media.INTERNAL_CONTENT_URI
        val projection = arrayOf(MediaStore.MediaColumns.DATA, MediaStore.Images.Media.BUCKET_DISPLAY_NAME, MediaStore.MediaColumns.DATE_MODIFIED)
        val cursorExternal = c.contentResolver.query(uriExternal, projection, "bucket_display_name = \"$album_name\"", null, null)
        val cursorInternal = c.contentResolver.query(uriInternal, projection, "bucket_display_name = \"$album_name\"", null, null)
        val cursor = MergeCursor(arrayOf(cursorExternal, cursorInternal))
        return cursor.count.toString()
    }

    override fun onPostExecute(xml: String) {
        albumManager.displayAlbums()
    }


}