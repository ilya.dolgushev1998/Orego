package com.orego.corporation.orego.models.portrait.personModel.exceptions;

/**
 * ViolatedBindingSequenceOfPartsException выпрыгивает, если Вы загружаете части головы в неправильном порядке
 */

public final class ViolatedBindingSequenceOfPartsException extends Exception {
}
