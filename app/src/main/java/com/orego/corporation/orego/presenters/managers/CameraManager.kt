package com.orego.corporation.orego.presenters.managers

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Matrix
import android.graphics.Point
import android.hardware.Camera
import android.os.Bundle
import android.os.Handler
import android.os.HandlerThread
import android.os.Looper
import android.util.Log
import android.view.Surface
import android.view.SurfaceHolder
import android.view.WindowManager
import com.orego.corporation.orego.presenters.MainActivity
import com.orego.corporation.orego.utils.faceDetection
import com.orego.corporation.orego.utils.setPreviewParams
import com.orego.corporation.orego.views.cameraFragment.CameraFragment
import java.util.*

class CameraManager(private val cameraFragment: CameraFragment) {

    companion object {
        private const val TAG = "CameraManager"
        private const val ROTATE_360 = 360
        private const val ROTATE_270 = 270
        private const val ROTATE_180 = 180
        private const val ROTATE_90 = 90
        private const val ROTATE_0 = 0

    }

    private var isFrontCamera = true
    private var isRunning = false
    private var cameraIdBack = -1
    private var cameraIdFront = -1
    private var mCamera: Camera? = null
    private val mUiHandler: Handler = Handler(Looper.getMainLooper())
    private val mThreadHandler: Handler
    private var mSurfaceHolder: SurfaceHolder? = null
    private val mSurfaceSize = Point()
    private var mCameraId: Int = 0
    private var mState = State.STATE_IDLE
    private val mSensorRotation: Int = 0
    private var isExpanded = false
    /*  */
    private var mPicture: Bitmap? = null

    private var isSurfaceCreated = false
    private val isOpened: Boolean
        get() = mCamera != null && mState != State.STATE_IDLE

    private val displayOrientation: Int
        get() {
            val info = Camera.CameraInfo()
            Camera.getCameraInfo(mCameraId, info)
            val windowManager = cameraFragment.context!!.getSystemService(Context.WINDOW_SERVICE) as WindowManager
            val rotation = Objects.requireNonNull(windowManager).defaultDisplay.rotation
            var degrees = 0
            when (rotation) {
                Surface.ROTATION_0 -> degrees = ROTATE_0
                Surface.ROTATION_90 -> degrees = ROTATE_90
                Surface.ROTATION_180 -> degrees = ROTATE_180
                Surface.ROTATION_270 -> degrees = ROTATE_270
            }

            var result: Int
            if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
                result = (info.orientation + degrees) % ROTATE_360
                result = (ROTATE_360 - result) % ROTATE_360
            } else {
                result = (info.orientation - degrees + ROTATE_360) % ROTATE_360
            }
            return result
        }

    private enum class State {
        STATE_IDLE,
        STATE_OPENED,
        STATE_SHOOTING
    }

    init {
        val handlerThread = HandlerThread("$TAG-Thread")
        handlerThread.start()
        mThreadHandler = Handler(handlerThread.looper)
        findCameras()
        mCameraId = -1
    }

    internal fun getIsExpanded(): Boolean {
        return isExpanded
    }

    fun expand() {
        isExpanded = true
        cameraFragment.playExpandAnimation()
    }

    private fun setSurfaceHolder(holder: SurfaceHolder?, width: Int, height: Int) {
        mSurfaceHolder = holder
        mSurfaceSize.set(height, width)
    }

    fun open() {
        if (!isOpened && isSurfaceCreated) {
            mThreadHandler.post(object : SafeRunnable() {
                override fun runSafely() {
                    openImmediate()
                    val success = mState == State.STATE_OPENED
                    mUiHandler.post {
                        if (!success) {
                            Exception("Error Camera opening")
                        }
                    }
                }
            })
        }
    }

    private fun openImmediate() {
        closeImmediate()

        if (mSurfaceHolder == null) {
            return
        }

        if (mCameraId < 0 && cameraIdBack >= 0) {
            mCameraId = cameraIdFront
        }

        if (mCameraId < 0) {
            return
        }

        try {
            mCamera = Camera.open(mCameraId)

            val parameters = mCamera!!.parameters
            setPreviewParams(mSurfaceSize, parameters)

            mCamera!!.parameters = parameters
            mCamera!!.setDisplayOrientation(displayOrientation)
            mCamera!!.setPreviewDisplay(mSurfaceHolder)
            mCamera!!.startPreview()
            setState(State.STATE_OPENED)
        } catch (th: Throwable) {
            Log.e(TAG, "open camera failed", th)
        }

    }

    internal fun isFrontCamera() = isFrontCamera

    fun hasMultiCamera(): Boolean {
        return cameraIdBack >= 0 && cameraIdFront >= 0
    }


    fun switchCamera() {
        mThreadHandler.post(object : SafeRunnable() {
            override fun runSafely() {
                if (!hasMultiCamera()) {
                    return
                }
                cameraFragment.switchCamIconAnimation()
                mCameraId = when (mCameraId) {
                    cameraIdBack -> {
                        isFrontCamera = true
                        cameraIdFront
                    }
                    cameraIdFront -> {
                        isFrontCamera = false
                        cameraIdBack
                    }
                    else -> cameraIdBack
                }

                openImmediate()

                val success = mState == State.STATE_OPENED
                mUiHandler.post { if (!success) Exception("switch camera failed") }
            }
        })
    }


    private fun takePicture() {
        isRunning = true
        mThreadHandler.post(object : SafeRunnable() {
            override fun runSafely() {
                if (mState != State.STATE_OPENED) {
                    isRunning = false
                    return
                }

                setState(State.STATE_SHOOTING)

                mCamera!!.takePicture(null, null) { data, _ ->
                    closeImmediate()

                    if (data != null && data.isNotEmpty()) {
                        val bitmap = BitmapFactory.decodeByteArray(data, 0, data.size)
                        val matrix = Matrix()
                        var rotation = displayOrientation + mSensorRotation
                        if (mCameraId == cameraIdBack) {
                            matrix.setRotate(rotation.toFloat())
                        } else {
                            rotation = (ROTATE_360 - rotation) % ROTATE_360
                            matrix.setRotate(rotation.toFloat())
                            matrix.postScale(-1f, 1f)
                        }
                        mPicture = Bitmap.createBitmap(bitmap, 0, 0, bitmap.width, bitmap.height, matrix, true)
                    } else {
                        mPicture = null
                    }
                    if (faceDetection(mPicture!!)) {
                        mUiHandler.post {
                            cameraFragment.playExpandAnimation()
                            cameraFragment.postCaptureClick(mPicture)
                            isRunning = false
                        }
                    } else {
                        openImmediate()
                        mUiHandler.post {
                            cameraFragment.showFaceNotFound()
                            mPicture = null
                        }
                    }

                }
            }
        })
    }

    fun close() {
        if (isOpened) {
            mThreadHandler.post(object : SafeRunnable() {
                override fun runSafely() {
                    closeImmediate()
                }
            })
        }
    }


    private fun closeImmediate() {
        if (mCamera != null) {
            mCamera!!.stopPreview()
            mCamera!!.release()
            mCamera = null
        }

        if (mState != State.STATE_IDLE) {
            setState(State.STATE_IDLE)
        }
    }

    private fun setState(state: State) {
        Log.d(TAG, "change state from $mState to $state")
        mState = state
    }

    private fun findCameras() {
        val info = Camera.CameraInfo()
        for (i in 0 until Camera.getNumberOfCameras()) {
            Camera.getCameraInfo(i, info)
            if (info.facing == Camera.CameraInfo.CAMERA_FACING_BACK) {
                cameraIdBack = info.facing
            } else if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
                cameraIdFront = info.facing
            }
        }
    }

    fun setIsSurfaceCreated(isSurfaceCreated: Boolean) {
        this.isSurfaceCreated = isSurfaceCreated
    }

    fun surfaceChanged(holder: SurfaceHolder, width: Int, height: Int) {
        setSurfaceHolder(holder, width, height)
        if (isOpened) {
            close()
        }
        open()
    }

    fun surfaceDestroyed() {
        isSurfaceCreated = false
        setSurfaceHolder(null, 0, 0)
    }

    fun onOkClick() {
        if (mPicture != null) {
                val bundle = Bundle()
                bundle.putParcelable("picture", mPicture)
                bundle.putInt("position", -1)
                (cameraFragment.activity as MainActivity).initModelFragment(bundle)
        } else {
            cameraFragment.showWarning()
        }
    }

    fun onBackClick() {
        if (!isRunning) {
            this.fold()
        } else {
            cameraFragment.showWarning()
        }
    }

    fun onCaptureClick() {
        takePicture()
    }

    fun fold() {
        isExpanded = false
        mPicture = null
        cameraFragment.hidePictureView()
        cameraFragment.fold()
    }

    private abstract class SafeRunnable : Runnable {
        companion object {
            const val TAG_SAFE_RUNNABLE = "SAFE_RUNNABLE"
        }

        override fun run() {
            try {
                runSafely()
            } catch (th: Throwable) {
                Log.e(TAG_SAFE_RUNNABLE, "camera error", th)
            }

        }

        abstract fun runSafely()
    }
}