package com.orego.corporation.orego.models.loaders.loadersAlbum

import android.database.MergeCursor
import android.os.AsyncTask
import android.provider.MediaStore
import com.orego.corporation.orego.models.loaders.loadersAlbum.supplier.MapComparator
import com.orego.corporation.orego.presenters.MainActivity
import com.orego.corporation.orego.presenters.managers.AlbumManager
import com.orego.corporation.orego.utils.KEY_TIMESTAMP
import com.orego.corporation.orego.utils.convertToTime
import com.orego.corporation.orego.utils.mappingInbox
import java.util.*


class LoadAlbumImages(private val albumManager: AlbumManager
                      , private val albumName: String) : AsyncTask<String, Void, String>() {

    private var imageList = ArrayList<HashMap<String, String>>()
    override fun onPreExecute() {
        super.onPreExecute()
        imageList.clear()
    }

    override fun doInBackground(vararg args: String): String {
        val xml = ""
        var path: String?
        var album: String?
        var timestamp: String?
        val uriExternal = MediaStore.Images.Media.EXTERNAL_CONTENT_URI
        val uriInternal = MediaStore.Images.Media.INTERNAL_CONTENT_URI

        val projection = arrayOf(MediaStore.MediaColumns.DATA, MediaStore.Images.Media.BUCKET_DISPLAY_NAME, MediaStore.MediaColumns.DATE_MODIFIED)

        val selection = "bucket_display_name = \"$albumName\""
        val mainActivity = albumManager.cameraFragment.activity as MainActivity
        val cursorExternal = mainActivity.contentResolver.query(uriExternal, projection, selection, null, null)
        val cursorInternal = mainActivity.contentResolver.query(uriInternal, projection, selection, null, null)
        val cursor = MergeCursor(arrayOf(cursorExternal, cursorInternal))
        while (cursor.moveToNext()) {
            path = cursor.getString(cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA))
            album = cursor.getString(cursor.getColumnIndexOrThrow(MediaStore.Images.Media.BUCKET_DISPLAY_NAME))
            timestamp = cursor.getString(cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATE_MODIFIED))
            imageList.add(mappingInbox(album, path, timestamp, convertToTime(timestamp), null))
        }
        cursor.close()
        Collections.sort(imageList, MapComparator(KEY_TIMESTAMP, "dsc")) // Arranging photo album by timestamp decending
        return xml
    }

    override fun onPostExecute(xml: String) {
        albumManager.displayImages(imageList)

    }
}