package com.orego.corporation.orego.utils

import android.graphics.Bitmap
import android.media.FaceDetector
import android.util.Log

/**
 * Detected face on the picture or photo
 * Need for successful create 3D FaceModel by image
 * @return true if find more 0 face
 */
fun faceDetection(bitmap: Bitmap) : Boolean{
    val mFaceBitmap = bitmap.copy(Bitmap.Config.RGB_565, true)
    val mFaceWidth = mFaceBitmap.width
    val mFaceHeight = mFaceBitmap.height
    val faces = arrayOfNulls<FaceDetector.Face>(1)
    val fd = FaceDetector(mFaceWidth, mFaceHeight, 1)
    val count = fd.findFaces(mFaceBitmap, faces) // find faces
    Log.d("FIND_FACES", "count = $count")
    return count > 0
}