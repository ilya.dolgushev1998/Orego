package com.orego.corporation.orego.presenters

import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.Window
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.orego.corporation.orego.R
import com.orego.corporation.orego.presenters.supplier.PermissionsDelegate
import com.orego.corporation.orego.views.cameraFragment.CameraFragment
import com.orego.corporation.orego.views.modelFragment.ModelFragment

/**
 * Главное activity отображающая
 * либо фрагмент камеры либо фрагмент 3D модели
 * и содержащее контроллер этих фрагментов.
 */


class MainActivity : AppCompatActivity() {

    companion object {
        private const val TAG = "MAIN_ACTIVITY"
        private const val EXIT_PERMISSION_DENIED = 137
    }

    private lateinit var cameraFragment: CameraFragment
    private lateinit var modelFragment: ModelFragment

    private var idFragment: Int? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        Log.i(TAG, "Create Main Activity")

        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN)

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        checkPermissions()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu_switch_camera; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_switch_camera, menu)
        return true
    }

    private fun startCameraFragment() {
        Log.i(TAG, "init Fragments")
        cameraFragment = CameraFragment()
        replaceFragment(cameraFragment)
    }

    internal fun initModelFragment(bundle: Bundle) {
        modelFragment = ModelFragment()
        modelFragment.arguments = bundle
        replaceFragment(modelFragment)
    }

    private fun checkPermissions() {
        val permissionsDelegate = PermissionsDelegate(this)
        Log.i(TAG, "check permissions")
        if (!permissionsDelegate.hasCameraPermission()) {
            Log.i(TAG, "has denied permission")
            permissionsDelegate.requestCameraPermission()
            Log.i(TAG, " requested permissions")
        } else {
            Log.i(TAG, " all permissions granted")
            startCameraFragment()
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == PermissionsDelegate.REQUEST_PERMISSION && grantResults.size == 4) {
            for (result in grantResults) {
                if (result == PackageManager.PERMISSION_DENIED) {
                    Log.e(TAG, "Permission Denied")
                    signOut()
                    return
                }
            }
            Log.i(TAG, " request permissions granted")
            startCameraFragment()
        }
    }

    private fun replaceFragment(fragment: Fragment) {
        Log.i(TAG, "replace Fragment")
        supportFragmentManager.beginTransaction().replace(R.id.container, fragment).commit()
        idFragment = fragment.id
    }

    override fun onBackPressed() {

        if (idFragment == cameraFragment.id) {
            if (cameraFragment.getState() != BottomSheetBehavior.STATE_COLLAPSED) {
                val albumManager = cameraFragment.albumManager
                if (albumManager.getIsAlbumOpened()) {
                    albumManager.displayAlbums()
                } else
                    cameraFragment.setState(BottomSheetBehavior.STATE_COLLAPSED)
            } else {
                if (cameraFragment.cameraManager.getIsExpanded()) {
                    cameraFragment.cameraManager.fold()
                } else signOut()
            }
        } else {
            if (modelFragment.isDownload()) {
                modelFragment.showSnackBarWaiting()
                return
            }
            replaceFragment(cameraFragment)
        }
    }

    private fun signOut() {
        val intent = Intent(Intent.ACTION_MAIN)
        intent.addCategory(Intent.CATEGORY_HOME)
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
        this.startActivity(intent)
        this.finish()
        System.exit(EXIT_PERMISSION_DENIED)
    }
}
