package com.orego.corporation.orego.presenters.adapters.galleryAdapter

import android.graphics.drawable.Drawable
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.orego.corporation.orego.BuildConfig
import com.orego.corporation.orego.R
import com.orego.corporation.orego.utils.ClientMultipartFormPost
import com.orego.corporation.orego.views.cameraFragment.CameraFragment


open class DemoAdapter(private val count: Int, private val cameraFragment: CameraFragment)
    : RecyclerView.Adapter<DemoAdapter.ViewHolder>(), View.OnClickListener {

    private var mOnItemClickListener: OnItemClickListener? = null

    internal fun setOnItemClickListener(onItemClickListener: OnItemClickListener): DemoAdapter {
        this.mOnItemClickListener = onItemClickListener
        return this
    }

    override fun getItemViewType(position: Int): Int {
        return VIEW_TYPE_IMAGE
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        if (BuildConfig.DEBUG) {
            Log.e(TAG, "onCreateViewHolder: type:$viewType")
        }
        val v: View = LayoutInflater.from(parent.context).inflate(R.layout.item_horizontal_recycler
                , parent, false)
        v.setOnClickListener(this)
        return ViewHolder(v)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        if (BuildConfig.DEBUG) {
            Log.d(TAG, "onBindViewHolder: position:$position")
        }
        val cardView = holder.itemView as CardView
        val imageView = cardView.findViewById<ImageView>(R.id.image_model)
        val imageFile = ClientMultipartFormPost.getImageByPosition(position, cameraFragment.activity!!.cacheDir)
        if (imageFile != null) {
            imageView.setImageDrawable(Drawable.createFromPath(imageFile.absolutePath))
        }
        holder.itemView.tag = position
    }

    override fun getItemCount(): Int {
        return count
    }

    override fun onClick(v: View) {
        if (mOnItemClickListener != null) {
            mOnItemClickListener!!.onItemClick(v, v.tag as Int)
        }
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)


    internal interface OnItemClickListener {

        fun onItemClick(view: View, position: Int)

    }

    companion object {
        const val VIEW_TYPE_IMAGE = 0
        private const val TAG = "DemoAdapter"
    }
}
