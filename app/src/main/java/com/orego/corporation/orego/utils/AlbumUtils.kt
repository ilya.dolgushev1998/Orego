package com.orego.corporation.orego.utils

import java.util.*


const val KEY_ALBUM = "album_name"
const val KEY_PATH = "path"
const val KEY_TIMESTAMP = "timestamp"
const val KEY_TIME = "date"
const val KEY_COUNT = "date"

fun mappingInbox(album: String, path: String, timestamp: String, time: String, count: String?): HashMap<String, String> {
    val map = HashMap<String, String>()
    map[KEY_ALBUM] = album
    map[KEY_PATH] = path
    map[KEY_TIMESTAMP] = timestamp
    map[KEY_TIME] = time
    if (count != null)
        map[KEY_COUNT] = count
    return map
}