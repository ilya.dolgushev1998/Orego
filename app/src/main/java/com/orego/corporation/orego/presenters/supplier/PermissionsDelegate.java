package com.orego.corporation.orego.presenters.supplier;

import android.Manifest;
import android.app.Activity;
import android.content.pm.PackageManager;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

public class PermissionsDelegate {

    private final Activity activity;
    public static final int REQUEST_PERMISSION = 225;

    public PermissionsDelegate(Activity activity) {
        this.activity = activity;
    }

    public boolean hasCameraPermission() {
        int permissionCheckResult = ContextCompat.checkSelfPermission(
                activity,
                Manifest.permission.CAMERA
        );
        int permissionCheckStorageWrite = ContextCompat.checkSelfPermission(
                activity,
                Manifest.permission.WRITE_EXTERNAL_STORAGE
        );
        int permissionCheckStorageRead = ContextCompat.checkSelfPermission(
                activity,
                Manifest.permission.READ_EXTERNAL_STORAGE
        );
        int permissionCheckInternet = ContextCompat.checkSelfPermission(
                activity,
                Manifest.permission.INTERNET
        );
        return (permissionCheckResult == PackageManager.PERMISSION_GRANTED)
                && (permissionCheckStorageRead == PackageManager.PERMISSION_GRANTED)
                && (permissionCheckStorageWrite == PackageManager.PERMISSION_GRANTED)
                && (permissionCheckInternet == PackageManager.PERMISSION_GRANTED);
    }

    public void requestCameraPermission() {
        ActivityCompat.requestPermissions(
                activity,
                new String[]{Manifest.permission.CAMERA
                        , Manifest.permission.WRITE_EXTERNAL_STORAGE
                        , Manifest.permission.READ_EXTERNAL_STORAGE
                        , Manifest.permission.INTERNET},
                REQUEST_PERMISSION
        );
    }

}
