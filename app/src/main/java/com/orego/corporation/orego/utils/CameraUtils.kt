package com.orego.corporation.orego.utils

import android.content.Context
import android.graphics.ImageFormat
import android.graphics.Point
import android.hardware.Camera
import android.util.Log
import java.util.ArrayList

private const val TAG = "CameraUtils"

fun setPreviewParams(surfaceSize: Point, parameters: Camera.Parameters?) {
    if (surfaceSize.x <= 0 || surfaceSize.y <= 0 || parameters == null) {
        return
    }
    val previewSizeList = parameters.supportedPreviewSizes
    val previewSize = findProperSize(surfaceSize, previewSizeList)
    if (previewSize != null) {
        parameters.setPreviewSize(previewSize.width, previewSize.height)
        Log.d(TAG, "previewSize: width: " + previewSize.width + ", height: " + previewSize.height)
    }

    val pictureSizeList = parameters.supportedPictureSizes
    val pictureSize = findProperSize(surfaceSize, pictureSizeList)
    if (pictureSize != null) {
        parameters.setPictureSize(pictureSize.width, pictureSize.height)
        Log.d(TAG, "pictureSize: width: " + pictureSize.width + ", height: " + pictureSize.height)
    }

    val focusModeList = parameters.supportedFocusModes
    if (focusModeList != null && focusModeList.contains(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE)) {
        parameters.focusMode = Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE
    }

    val pictureFormatList = parameters.supportedPictureFormats
    if (pictureFormatList != null && pictureFormatList.contains(ImageFormat.JPEG)) {
        parameters.pictureFormat = ImageFormat.JPEG
        parameters.jpegQuality = 100
    }
}

private fun findProperSize(surfaceSize: Point, sizeList: List<Camera.Size>?): Camera.Size? {
    if (surfaceSize.x <= 0 || surfaceSize.y <= 0 || sizeList == null) {
        return null
    }

    val surfaceWidth = surfaceSize.x
    val surfaceHeight = surfaceSize.y

    val ratioListList = ArrayList<MutableList<Camera.Size>>()
    for (size in sizeList) {
        addRatioList(ratioListList, size)
    }

    val surfaceRatio = surfaceWidth.toFloat() / surfaceHeight
    var bestRatioList: List<Camera.Size>? = null
    var ratioDiff = java.lang.Float.MAX_VALUE
    for (ratioList in ratioListList) {
        val ratio = ratioList[0].width.toFloat() / ratioList[0].height
        val newRatioDiff = Math.abs(ratio - surfaceRatio)
        if (newRatioDiff < ratioDiff) {
            bestRatioList = ratioList
            ratioDiff = newRatioDiff
        }
    }

    var bestSize: Camera.Size? = null
    var diff = Integer.MAX_VALUE
    assert(bestRatioList != null)
    for (size in bestRatioList!!) {
        val newDiff = Math.abs(size.width - surfaceWidth) + Math.abs(size.height - surfaceHeight)
        if (size.height >= surfaceHeight && newDiff < diff) {
            bestSize = size
            diff = newDiff
        }
    }

    if (bestSize != null) {
        return bestSize
    }

    diff = Integer.MAX_VALUE
    for (size in bestRatioList) {
        val newDiff = Math.abs(size.width - surfaceWidth) + Math.abs(size.height - surfaceHeight)
        if (newDiff < diff) {
            bestSize = size
            diff = newDiff
        }
    }

    return bestSize
}

private fun addRatioList(ratioListList: MutableList<MutableList<Camera.Size>>, size: Camera.Size) {
    val ratio = size.width.toFloat() / size.height
    for (ratioList in ratioListList) {
        val mine = ratioList[0].width.toFloat() / ratioList[0].height
        if (ratio == mine) {
            ratioList. add(size)
            return
        }
    }
    val ratioList = ArrayList<Camera.Size>()
    ratioList.add(size)
    ratioListList.add(ratioList)
}

fun dp2px(context: Context, dpValue: Float): Int {
    val density = context.resources.displayMetrics.density
    return (dpValue * density + 0.5f).toInt()
}