package com.orego.corporation.orego.models.loaders.loadersModel

import android.graphics.Bitmap
import android.os.AsyncTask
import com.orego.corporation.orego.models.portrait.headComposition.HeadComposition
import com.orego.corporation.orego.utils.*
import com.orego.corporation.orego.views.modelFragment.ModelFragment
import java.io.File
import java.io.FileInputStream
import java.io.IOException

/**
 * Async loadModel and control states.
 */
class LoadModel(private val fragment: ModelFragment, private val mBitmap: Bitmap
                , private val position: Int) : AsyncTask<Void, Int, Boolean>() {

    companion object {
        const val TAG = "LOAD_MODEL"
    }

    private lateinit var headComposition: HeadComposition

    private var newDirectory: File? = null

    private val states = listOf("Готовим Ваше фото", "Делаем магию с Вашей фотографией"
            , "Рисуем ваше чудо-личико :)", "Готово")

    override fun onPreExecute() {
        super.onPreExecute()
        this.fragment.initDialog()
    }

    override fun doInBackground(vararg voids: Void): Boolean {
        publishProgress(0)
        try {
            val model: File?
            if (position == -1) {
                newDirectory = ClientMultipartFormPost.savePicture(mBitmap, fragment.activity!!.cacheDir)
                publishProgress(1)
                model = ClientMultipartFormPost.sendFile(newDirectory!!)
                publishProgress(2)
            } else {
                publishProgress(2)
                model = ClientMultipartFormPost.getBuffByPosition(position, fragment.activity!!.cacheDir)
                if (model == null) return false
            }
            val isFace = FileInputStream(model)
            headComposition = HeadComposition(isFace)
            return headComposition.initBuffers()
        } catch (e: IOException) {
            e.printStackTrace()
        }
        return false
    }

    override fun onProgressUpdate(vararg values: Int?) {
        super.onProgressUpdate(*values)
        this.fragment.updateDialog(states[values[0]!!])
    }

    override fun onPostExecute(boolean: Boolean) {
        super.onPostExecute(boolean)
        fragment.closeDialog()
        if (boolean) fragment.setModel(headComposition)
        else {
            fragment.showSnackBarError()
            //TODO: often server not found... remote logging
            if (position == -1) {
                fragment.activity!!.onBackPressed()
                ClientMultipartFormPost.deleteDir(newDirectory!!)
            } else {
                fragment.activity!!.onBackPressed()
                ClientMultipartFormPost.deleteDirByPosition(position, fragment.activity!!.cacheDir)
            }
        }

    }
}