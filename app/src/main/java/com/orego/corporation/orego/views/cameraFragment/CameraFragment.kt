package com.orego.corporation.orego.views.cameraFragment

import android.animation.Animator
import android.animation.ValueAnimator
import android.annotation.SuppressLint
import android.app.AlertDialog
import android.graphics.Bitmap
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.util.Log
import android.view.*
import android.view.View.GONE
import android.view.View.VISIBLE
import android.widget.*
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.core.widget.NestedScrollView
import androidx.fragment.app.Fragment
import com.google.android.material.bottomappbar.BottomAppBar
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.snackbar.Snackbar
import com.orego.corporation.orego.R
import com.orego.corporation.orego.R.id.horizontal_recycler
import com.orego.corporation.orego.presenters.MainActivity
import com.orego.corporation.orego.presenters.SwipeListener
import com.orego.corporation.orego.presenters.managers.AlbumManager
import com.orego.corporation.orego.presenters.managers.CameraManager
import com.orego.corporation.orego.utils.convertDpToPixel
import com.orego.corporation.orego.views.KOEFF_A
import com.orego.corporation.orego.views.KOEFF_B
import com.orego.corporation.orego.views.KOEFF_C
import com.orego.corporation.orego.views.KOEFF_HELF


/**
 * Main Fragment, implements camera view,
 * allow user take pictures.
 * layer-based view which contain gallery recyclerView,
 * album gridView
 */

class CameraFragment : Fragment(), SurfaceHolder.Callback {

    /**
     * Static const
     */
    companion object {
        private const val TAG = "CameraFragment"
        const val TIME_ANIM = 500L
    }

    /* views */
    private lateinit var mSurfaceView: SurfaceView
    private lateinit var mPictureView: ImageView
    private lateinit var btnInfo: ImageView
    private lateinit var mBottomSheetBehavior: BottomSheetBehavior<*>
    private lateinit var buttonCollapse: ImageView
    private lateinit var galleryGridView: GridView
    private lateinit var bar: BottomAppBar
    private lateinit var fab: FloatingActionButton
    private lateinit var fabOk: FloatingActionButton
    private lateinit var fabBack: FloatingActionButton

    /* managers */
    internal lateinit var cameraManager: CameraManager
    internal lateinit var albumManager: AlbumManager

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?
                              , savedInstanceState: Bundle?): View {
        Log.d(TAG, "CreateView")
        val coordinatorLayout = inflater.inflate(R.layout.camera_fragment, container
                , false) as CoordinatorLayout
        this.initViews(coordinatorLayout)
        this.initCallBacks()
        this.initManagers()
        this.initListeners()
        return coordinatorLayout
    }

    private fun initManagers() {
        this.cameraManager = CameraManager(this)
        this.albumManager = AlbumManager(this)
    }

    private fun initViews(view: View) {
        val bottomSheet = view.findViewById<View>(R.id.bottom_sheet) as NestedScrollView
        this.mBottomSheetBehavior = BottomSheetBehavior.from(bottomSheet)
        this.bar = view.findViewById(R.id.bar) as BottomAppBar
        (activity as MainActivity).setSupportActionBar(bar)
        this.fab = view.findViewById(R.id.fab)
        this.fabOk = view.findViewById(R.id.fab_ok)
        this.fabBack = view.findViewById(R.id.fab_close)
        this.mSurfaceView = view.findViewById<View>(R.id.camera_surface) as SurfaceView
        this.mPictureView = view.findViewById<View>(R.id.camera_picture_preview) as ImageView
        this.galleryGridView = view.findViewById(R.id.galleryGridView) as GridView
        this.buttonCollapse = view.findViewById<View>(R.id.btn_sheet_close) as ImageView
        this.btnInfo = view.findViewById(R.id.btn_info)
    }

    @SuppressLint("ClickableViewAccessibility")
    private fun initListeners() {
        this.buttonCollapse.setOnClickListener {
            setState(BottomSheetBehavior.STATE_COLLAPSED)
        }
        this.bar.setOnMenuItemClickListener {
            // Handle actions based on the menu_switch_camera item
            this.cameraManager.switchCamera()
            true
        }
        this.bar.setNavigationOnClickListener {
            setState(BottomSheetBehavior.STATE_EXPANDED)
        }
        this.fab.setOnClickListener { this.cameraManager.onCaptureClick() }
        this.fabOk.setOnClickListener { this.cameraManager.onOkClick() }
        this.fabBack.setOnClickListener { this.cameraManager.onBackClick() }
        this.mSurfaceView.setOnTouchListener(SwipeListener(this))

        /* init alert dialog with listeners*/
        val dialogLayout = layoutInflater.inflate(R.layout.custom_dialog, null)
        val alert = AlertDialog.Builder(context).setView(dialogLayout).create()
        val window = alert.window!!
        window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        window.setDimAmount(0F)
        this.btnInfo.setOnClickListener { _ ->
            infoClick(alert)
            val button = alert.findViewById<Button>(R.id.close_dialog)
            button.setOnClickListener { alert.hide() }
        }
    }

    private fun initCallBacks() {
        mBottomSheetBehavior.setBottomSheetCallback(object : BottomSheetBehavior.BottomSheetCallback() {
            override fun onStateChanged(bottomSheet: View, newState: Int) {
                if (newState == BottomSheetBehavior.STATE_COLLAPSED) {
                    Log.d(TAG, "showAnimation bar")
                    showAnim()
                } else {
                    hideAnim()
                }
            }

            override fun onSlide(bottomSheet: View, slideOffset: Float) {
            }
        })
        mSurfaceView.holder.addCallback(this)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        Log.d(TAG, "viewCreated")
        super.onViewCreated(view, savedInstanceState)

        val mMainRecycle = view.findViewById<androidx.recyclerview.widget.RecyclerView>(horizontal_recycler)
        val iDisplayWidth = resources.displayMetrics.widthPixels
        val applicationContext = activity!!.applicationContext
        val resourcesActivity = applicationContext.resources
        val metrics = resourcesActivity.displayMetrics
        var dp = iDisplayWidth / (metrics.densityDpi / KOEFF_A)

        if (dp < KOEFF_B) {
            dp = (dp - KOEFF_C) / KOEFF_HELF
            val px = convertDpToPixel(dp, applicationContext)
            galleryGridView.columnWidth = Math.round(px)
        }
        albumManager.fillRecycle(mMainRecycle)
    }


    override fun onResume() {
        super.onResume()
        Log.d(TAG, "onResume")

        cameraManager.open()
    }


    override fun onPause() {
        super.onPause()
        Log.d(TAG, "onPause")

        cameraManager.close()
    }

    fun postCaptureClick(result: Bitmap?) {
        Log.d(TAG, "CaptureClick")
        if (result != null) {
            mSurfaceView.visibility = GONE
            btnInfo.visibility = GONE
            mPictureView.visibility = VISIBLE
            mPictureView.setImageBitmap(result)
            cameraManager.expand()
        } else {
            cameraManager.onBackClick()
        }
    }


    fun hidePictureView() {
        mPictureView.visibility = GONE
        btnInfo.visibility = VISIBLE
        mSurfaceView.visibility = VISIBLE
    }

    private fun infoClick(alert: AlertDialog) {
        Log.d(TAG, "info click")
        alert.show()
    }


    fun getState(): Int {
        return mBottomSheetBehavior.state
    }

    fun setState(state: Int) {
        mBottomSheetBehavior.state = state
    }

    /**
     * surface implemetation
     */
    override fun surfaceCreated(holder: SurfaceHolder) {
        Log.d(TAG, "surfaceCreated")
        cameraManager.setIsSurfaceCreated(true)
    }


    override fun surfaceChanged(holder: SurfaceHolder, format: Int, width: Int, height: Int) {
        Log.d(TAG, "surfaceChanged")
        cameraManager.surfaceChanged(holder, width, height)
    }

    override fun surfaceDestroyed(holder: SurfaceHolder) {
        Log.d(TAG, "surfaceDestroyed")
        cameraManager.surfaceDestroyed()
    }

    /**
     * Prep: Two Button to One
     */
    internal fun fold() {
        fabBack.hide()
        fabOk.hide()
        showAnim()
    }


    /**
     * Animation button.
     * Button Take Picture duplicates to two btn:
     * Button retry and Button ok
     */
    fun playExpandAnimation() {
        hideAnim()
        fabBack.show()
        fabOk.show()
    }

    private fun hideAnim() {
        fab.hide()
        animateVisibility(0f, bar)
        animateVisibility(0f, btnInfo)
    }

    private fun showAnim() {
        fab.show()
        animateVisibility(1f, bar)
        animateVisibility(1f, btnInfo)
    }


    fun showWarning() {
        Snackbar.make(fabOk, "Saving image...", Snackbar.LENGTH_SHORT).show()
    }

    fun showFaceNotFound() {
        Snackbar.make(galleryGridView, "Face not found on this picture", Snackbar.LENGTH_SHORT).show()

    }

    fun switchCamIconAnimation() {

        val view = bar.menu.findItem(R.id.app_bar_fav)
        val valueAnimator = ValueAnimator.ofInt(255, 0)
        valueAnimator.addUpdateListener {
            activity!!.runOnUiThread {
                val value = it.animatedValue as Int
                view.icon.alpha = value
            }
        }
        val valueAnimator2 = ValueAnimator.ofInt(0, 255)
        valueAnimator2.addUpdateListener {
            activity!!.runOnUiThread {
                val value = it.animatedValue as Int
                view.icon.alpha = value
            }
        }
        valueAnimator.addListener(object : Animator.AnimatorListener {
            override fun onAnimationRepeat(animation: Animator?) {
            }

            override fun onAnimationEnd(animation: Animator?) {
                activity!!.runOnUiThread {
                    if (cameraManager.isFrontCamera()) {
                        view.icon = this@CameraFragment.resources.getDrawable(R.drawable.ic_camera_switch_front, null)
                    } else {
                        view.icon = this@CameraFragment.resources.getDrawable(R.drawable.ic_camera_switch, null)
                    }
                    valueAnimator2.start()
                }
            }

            override fun onAnimationCancel(animation: Animator?) {
            }

            override fun onAnimationStart(animation: Animator?) {
            }
        })
        valueAnimator2.duration = TIME_ANIM / 2
        valueAnimator.duration = TIME_ANIM / 2
        valueAnimator.start()
    }

    private fun animateVisibility(to: Float, view: View) {
        Log.d(TAG, "animate show to $to")
        val valueAnimator = ValueAnimator.ofFloat(view.alpha, to)
        valueAnimator.addUpdateListener {
            val value = it.animatedValue as Float
            view.alpha = value
            Log.d(TAG, "animateValue$value")
        }
        valueAnimator.duration = TIME_ANIM
        valueAnimator.addListener(object : Animator.AnimatorListener{
            override fun onAnimationRepeat(p0: Animator?) {
            }

            override fun onAnimationEnd(p0: Animator?) {
            }

            override fun onAnimationCancel(p0: Animator?) {
            }

            override fun onAnimationStart(p0: Animator?) {
            }

        })
        valueAnimator.start()
    }

    fun initGridView(adapter: BaseAdapter, clickListener: AdapterView.OnItemClickListener ) {
        galleryGridView.adapter = adapter
        galleryGridView.onItemClickListener = clickListener
    }

}