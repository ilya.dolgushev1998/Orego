package com.orego.corporation.orego.utils

import android.graphics.Bitmap
import android.util.Log
import org.apache.commons.io.FileUtils
import java.io.*
import java.net.HttpURLConnection
import java.net.URL
import java.util.zip.ZipEntry
import java.util.zip.ZipInputStream

object ClientMultipartFormPost {

    private const val TAG = "ClientMultipartFormPost"
    private const val QUALITY_COMPRESS = 100


    fun savePicture(bitmap: Bitmap, cacheDir: File): File {
        Log.d(TAG, bitmap.toString())

        val newDirectory = getNewDirectory(cacheDir)
        val saveImage = File(newDirectory, "/result.jpg")
        try {
            val out = FileOutputStream(saveImage)
            bitmap.compress(Bitmap.CompressFormat.JPEG, QUALITY_COMPRESS, out)
            out.flush()
            out.close()
        } catch (e: IOException) {
            Log.e(TAG, "save picture error", e)
        }
        return newDirectory
    }

    fun deleteDir(file: File) {
        FileUtils.deleteDirectory(file)
    }

    fun deleteDirByPosition(position: Int, file: File) {
        FileUtils.deleteDirectory(File(file, "/OREGO/directory$position"))
    }

    @Throws(IOException::class)
    fun sendFile(saveDir: File): File {
        val url = URL("https://face.spbpu.com/servletpost/")
        val image = File(saveDir, "/result.jpg")
        val con = url.openConnection() as HttpURLConnection
        con.requestMethod = "POST"
        con.useCaches = false
        con.doOutput = true
        con.doInput = true
        con.addRequestProperty("3dfacePOST", "3dfacePOST")
        con.connect()
        var bis = BufferedInputStream(FileInputStream(image))
        var bos = BufferedOutputStream(con.outputStream)
        var byteArray = ByteArray(1)
        while (bis.read(byteArray) != -1) {
            bos.write(byteArray, 0, 1)
        }

        bis.close()
        bos.close()
        val model = File(saveDir,
                "serverResponse.buf.zip")
        bis = BufferedInputStream(con.inputStream)
        bos = BufferedOutputStream(FileOutputStream(model))
        byteArray = ByteArray(1)
        while (con.inputStream.read(byteArray, 0, 1) != -1) {
            bos.write(byteArray, 0, 1)
        }
        bis.close()
        bos.close()
        val resultFile = File(saveDir,
                "resultObj.buf")
        return unzipFunction(resultFile, model)
    }

    private fun unzipFunction(resultFile: File, zipFile: File): File {
        val buffer = ByteArray(1024)
        val zis: ZipInputStream
        try {
            zis = ZipInputStream(FileInputStream(zipFile))
            var zipEntry: ZipEntry? = zis.nextEntry
            while (zipEntry != null) {
                val fos = FileOutputStream(resultFile)
                var len = zis.read(buffer)
                while (len > 0) {
                    fos.write(buffer, 0, len)
                    len = zis.read(buffer)
                }
                fos.close()
                zipEntry = zis.nextEntry
            }
            zis.closeEntry()
            zis.close()
        } catch (e: IOException) {
            e.printStackTrace()
        }
        return resultFile
    }

    /**
     * @return dir to save resultObj
     * @sample OregoCache/OREGO/directory$i/
     */
    private fun getNewDirectory(cacheDir: File): File {
        val orego = File(cacheDir, "/OREGO")
        if (!orego.exists()) orego.mkdir()
        val count = getCountDirectories(cacheDir)
        val dir = File(orego, "/directory$count")
        dir.mkdir()
        return dir
    }


    /**
     * @return count dir in cacheDir
     * @sample directory for finder: OregoCache/OREGO
     */
    fun getCountDirectories(cacheDir: File): Int {
        val orego = File(cacheDir, "/OREGO")
        if (!orego.exists()) orego.mkdir()
        var count = -1
        do {
            count++
            val dir = File(orego, "/directory$count")
        } while (dir.exists())

        return count
    }


    fun getImageByPosition(position: Int, cacheDir: File): File? {
        val orego = File(cacheDir, "/OREGO")
        if (!orego.exists()) orego.mkdir()
        val dir = File(orego, "/directory$position")
        if (dir.exists()) {
            val image = File(dir, "result.jpg")
            if (image.exists()) return image
        }
        return null
    }

    fun getBuffByPosition(position: Int, cacheDir: File): File? {
        val orego = File(cacheDir, "/OREGO")
        if (!orego.exists()) orego.mkdir()
        val dir = File(orego, "/directory$position")
        if (dir.exists()) {
            val image = File(dir, "resultObj.buf")
            if (image.exists()) return image
        }
        return null
    }
}