package com.orego.corporation.orego.presenters.managers

import android.graphics.BitmapFactory
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ImageView
import com.orego.corporation.orego.models.loaders.loadersAlbum.LoadAlbum
import com.orego.corporation.orego.models.loaders.loadersAlbum.LoadAlbumImages
import com.orego.corporation.orego.presenters.managers.supplier.ScaleTransformer
import com.orego.corporation.orego.presenters.MainActivity
import com.orego.corporation.orego.presenters.adapters.albumAdapter.AlbumAdapter
import com.orego.corporation.orego.presenters.adapters.albumAdapter.SingleAlbumAdapter
import com.orego.corporation.orego.presenters.adapters.galleryAdapter.DemoAdapter
import com.orego.corporation.orego.utils.ClientMultipartFormPost
import com.orego.corporation.orego.utils.KEY_ALBUM
import com.orego.corporation.orego.utils.faceDetection
import com.orego.corporation.orego.views.cameraFragment.CameraFragment
import java.io.File
import java.util.*


class AlbumManager(internal val cameraFragment: CameraFragment) {

    /* check openedAlbum */
    private var isAlbumOpened: Boolean = false
    /* caches albums */
    private var albumList = ArrayList<HashMap<String, String>>()

    init {
        loadAlbums()
    }

    /**
     * Async load all Albums from Device
     * postLoading displayAlbums
     * */
    private fun loadAlbums() {
        val loadAlbumTask = LoadAlbum(this, albumList)
        loadAlbumTask.execute()
    }

    /**
     * DisplayAlbums from caches albumList
     *
     */
    internal fun displayAlbums() {
        isAlbumOpened = false
        val mainActivity = cameraFragment.activity as MainActivity
        val adapter = AlbumAdapter(mainActivity, albumList)
        cameraFragment.initGridView(adapter, AdapterView.OnItemClickListener { _, _, position, _ ->
            val albumName = albumList[+position][KEY_ALBUM]!!
            val loadAlbumTask = LoadAlbumImages(this@AlbumManager, albumName)
            loadAlbumTask.execute()
            isAlbumOpened = true
        })
    }

    /**
     * DisplayImages
     *
     */
    internal fun displayImages(imageList: ArrayList<HashMap<String, String>>) {
        val mainActivity = cameraFragment.activity as MainActivity
        val adapter = SingleAlbumAdapter(mainActivity, imageList)
        cameraFragment.initGridView(adapter, AdapterView.OnItemClickListener{ _, _, position, id ->
            val imgFile = File(imageList[position]["path"])
            if (imgFile.exists()) {
                val myBitmap = BitmapFactory.decodeFile(imgFile.absolutePath)
                val imageView = mainActivity.findViewById<ImageView>(id.toInt())
                imageView.setImageBitmap(myBitmap)
                if (faceDetection(myBitmap)) {
                    val bundle = Bundle()
                    bundle.putParcelable("picture", myBitmap)
                    bundle.putInt("position", -1)
                    mainActivity.initModelFragment(bundle)
                } else {
                    faceDetectionNotify()
                }
            }
        })
    }

    /**
     * getStateAlbum
     * State: opened = true / closed = false
     */
    internal fun getIsAlbumOpened(): Boolean {
        return isAlbumOpened
    }

    /**
     * Fill Horizontal RecycleView
     * this recycle store loader models
     */
    fun fillRecycle(mMainRecycle: androidx.recyclerview.widget.RecyclerView) {
        val size = ClientMultipartFormPost.getCountDirectories(cameraFragment.activity!!.cacheDir)
        val layoutManager = GalleryLayoutManager(GalleryLayoutManager.HORIZONTAL)
        layoutManager.attach(mMainRecycle, size)
        layoutManager.setItemTransformer(ScaleTransformer())
        val demoAdapter = object : DemoAdapter(size, cameraFragment) {
        }
        demoAdapter.setOnItemClickListener(object : DemoAdapter.OnItemClickListener {
            override fun onItemClick(view: View, position: Int) {
                val bundle = Bundle()
                val cardView = view as androidx.cardview.widget.CardView
                val imageView = cardView.getChildAt(0) as ImageView
                imageView.buildDrawingCache()
                bundle.putParcelable("picture", imageView.drawingCache)
                bundle.putInt("position", position)
                val mainActivity = cameraFragment.activity as MainActivity
                mainActivity.initModelFragment(bundle)
            }

        })
        mMainRecycle.setHasFixedSize(true)
        mMainRecycle.adapter = demoAdapter
        mMainRecycle.layoutManager = layoutManager
    }

    private fun faceDetectionNotify() {
        cameraFragment.showFaceNotFound()
    }
}